#! /bin/bash

ips_url='/home/user/ipset_config_script/ips.txt'
ipset_setname='block_ips'

ipset list $ipset_setname >/dev/null 2>&1
if [[ $? -ne 0 ]]; then
  ipset create $ipset_setname hash:ip hashsize 16777216 maxelem 16777216

fi

readarray -t ips_array < /home/admin/ipset_config_script/ips.txt
#ips_array_len=${#ips_array[@]}
#echo "ips_array_len: " $ips_array_len

# Add IPs
for ip in "${ips_array[@]}"
do
  if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3} ]] || [[ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\/[0-9]{1,3} ]] ; then
    ipset -exist add $ipset_setname $ip
  fi
done

iptables -A INPUT -m set --match-set block_ips src -j DROP

exit 0