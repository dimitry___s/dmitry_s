#! /bin/bash

#
#
#Spark-Elastic configuration script
#
#

#define variables
#Enter different location for INSTALL_DIR if you need to
INSTALL_DIR=/opt/spark-elastic-inst
START_DIR=$PWD
LOG_FILE=$START_DIR/spark-elastic-install.log

#This function is used to check the result of executed commands
#if result is ok, then script execution continued,
#else script execution stops
function run (){
    "$@"
    local status=$?
    if [ $status -ne 0 ]; then
        echo "Something went wrong with the script, please check log file "$LOG_FILE" for more info!"| tee /dev/fd/3
        exit $status
    fi
    return $status
}

#This comand is used to work with log output, only few messages go to console (using | tee /dev/fd/3),
#most info goes to log file
exec 3>&1 1>>${LOG_FILE} 2>&1

echo "#######" | tee /dev/fd/3
echo "#######" | tee /dev/fd/3
echo "#######" | tee /dev/fd/3
echo "spark-elastic install script" | tee /dev/fd/3
echo "Starting install proccess..." | tee /dev/fd/3
echo "Full install log is "$START_DIR/spark-elastic-install.log | tee /dev/fd/3
echo "#######" | tee /dev/fd/3
echo "#######" | tee /dev/fd/3
echo "#######" | tee /dev/fd/3

#Check rights, must be root to run the script
echo "Checking current user rights..." | tee /dev/fd/3
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root!" | tee /dev/fd/3
   exit 1
fi
echo "User rights ok..." | tee /dev/fd/3

#Update centos
echo "Starting centos update..." | tee /dev/fd/3
run yum update -y
echo "Centos update done...ok" | tee /dev/fd/3

#Make directories for install files and make directory for spark,mavem,kafka
echo "Checking directories for install..." | tee /dev/fd/3
if [ ! -d "$INSTALL_DIR" ]; then
  run mkdir $INSTALL_DIR
fi
if [ ! -d "/usr/local/apache" ]; then
  run mkdir /usr/local/apache
fi

#Install scala
cd $INSTALL_DIR
echo "Starting scala installation..." | tee /dev/fd/3
#Using Variables like SCALA_VERSION you can update script faster if you need to
SCALA_VERSION=2.11.7
run wget http://downloads.typesafe.com/scala/$SCALA_VERSION/scala-$SCALA_VERSION.tgz
run tar -zxvf scala-$SCALA_VERSION.tgz
run mv scala-$SCALA_VERSION /usr/local/lib/
cd /usr/local/lib/
run ln -s scala-$SCALA_VERSION scala
cd /usr/local/bin
run ln -s /usr/local/lib/scala/bin/scala ./
cd $INSTALL_DIR
echo "Scala installation done... ok" | tee /dev/fd/3

#Install Kafka
echo "Starting kafka installation..." | tee /dev/fd/3
KAFKA_VERSION=0.9.0.0
run wget http://www.us.apache.org/dist/kafka/$KAFKA_VERSION/kafka_2.11-$KAFKA_VERSION.tgz
run tar -zxvf kafka_2.11-$KAFKA_VERSION.tgz
run mv kafka_2.11-$KAFKA_VERSION /usr/local/apache/
cd /usr/local/apache/
run ln -s kafka_2.11-$KAFKA_VERSION kafka
cd $INSTALL_DIR
echo "Kafka installation done... ok" | tee /dev/fd/3

#Install Apache Spark
echo "Starting spark installation..." | tee /dev/fd/3
SPARK_VERSION=1.5.2
run wget http://www.us.apache.org/dist/spark/spark-$SPARK_VERSION/spark-$SPARK_VERSION-bin-hadoop2.6.tgz 
run tar -zxvf spark-$SPARK_VERSION-bin-hadoop2.6.tgz
run mv spark-$SPARK_VERSION-bin-hadoop2.6 /usr/local/apache/
cd /usr/local/apache/
run ln -s spark-$SPARK_VERSION-bin-hadoop2.6 spark
cd $INSTALL_DIR
echo "Spark installation done... ok" | tee /dev/fd/3

# Maven Install
echo "Starting maven installation..." | tee /dev/fd/3
MAVEN_VERSION=3.3.9
run wget http://www.us.apache.org/dist/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz
run tar -zxvf apache-maven-$MAVEN_VERSION-bin.tar.gz
run mv apache-maven-$MAVEN_VERSION /usr/local/apache/
cd /usr/local/apache/
run ln -s apache-maven-$MAVEN_VERSION maven
cd /usr/local/bin
run ln -s /usr/local/apache/maven/bin/mvn ./
echo "Maven installation done...ok" | tee /dev/fd/3
cd $INSTALL_DIR

#Check if user apache exist already
if ! id -u "apache" >/dev/null 2>&1; then
        #If not, add user apache
    echo "Adding user apache..." | tee /dev/fd/3
    run useradd apache
    echo "User apache added...ok" | tee /dev/fd/3
fi

#Change rights from root to user apache
echo "Changing rights on /usr/local/apache..." | tee /dev/fd/3
run chown -R apache:apache /usr/local/apache
echo "Rights changed...ok" | tee /dev/fd/3

#Configuration of systemd scripts,services start + autostart
echo "Adding systemd scripts..." | tee /dev/fd/3
run cp $START_DIR/zookeeper.service /etc/systemd/system/zookeeper.service
run cp $START_DIR/kafka.service /etc/systemd/system/kafka.service
run cp $START_DIR/spark-master.service /etc/systemd/system/spark-master.service
echo "Scripts added...ok" | tee /dev/fd/3
echo "Starting and enabling services..." | tee /dev/fd/3
run systemctl start zookeeper.service
run systemctl start kafka.service
run systemctl enable zookeeper.service
run systemctl enable kafka.service
run systemctl start spark-master.service
run systemctl enable spark-master.service
echo "Services are started and enabled...ok" | tee /dev/fd/3

# Install Elastic repo & elasticsearch
echo "Starting elasticsearch installation..." | tee /dev/fd/3
run rpm --import https://packages.elastic.co/GPG-KEY-elasticsearch
run cp $START_DIR/elasticsearch.repo /etc/yum.repos.d/elasticsearch.repo
sleep 2
run yum install elasticsearch -y
run systemctl enable elasticsearch.service
run systemctl start elasticsearch.service
echo "Elasticsearch install done...ok" | tee /dev/fd/3
cd $INSTALL_DIR

echo "Starting spark-elastic compilation..." | tee /dev/fd/3
run git clone https://github.com/skrusche63/spark-elastic.git /usr/local/lib/spark-elastic
cd /usr/local/lib/spark-elastic
run /usr/local/apache/maven/bin/mvn install
cd $INSTALL_DIR
echo "spark-elastic deployment done...ok" | tee /dev/fd/3
echo "spark-elastic.jar is located in /root/.m2/repository/spark-elastic/spark-elastic/" | tee /dev/fd/3
echo "Script completed!" | tee /dev/fd/3
exit 0
